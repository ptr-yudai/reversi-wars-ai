import sys

b = int(sys.argv[1])
w = int(sys.argv[2])

for i in range(64):
    if ((b >> i) & 1) == 1:
        sys.stdout.write("B")
    elif ((w >> i) & 1) == 1:
        sys.stdout.write("W")
    else:
        sys.stdout.write(".")
    if i % 8 == 7: print("")
