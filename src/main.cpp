#include "board.hpp"
#include "client.hpp"
#include "ai.hpp"

#define DEBUG

int release();
int debug();

/*
  {"action":"register", "userinfo":{"username":"ptr", "password":"r3v3r51"}}
 */

int main()
{
#ifdef DEBUG
  debug();
#else
  release();
#endif
}


int release(void)
{
  bool isBlack = true;

  // 接続
  Client *sock = new Client("theoldmoon0602.tk", 8888);
  sock->Connect();
  // ログイン
  if (!sock->Login("ptr", "r3v3r51")) {
    return 1;
  }

  // 対戦相手決定
  std::cout << "[1]Standby / [2]Match" << std::endl;
  int c = -1;
  while((c != 1) && (c != 2)) {
    std::cin >> c;
  }
  std::string opponent = "";
  bool isStandby = false;
  if (c == 1) {
    // 待機
    isStandby = true;
  } else {
    // 対戦
    std::cout << "Your Opponent: ";
    std::cin >> opponent;
  }
  if (!sock->Match(opponent, isStandby, &isBlack)) {
    return 1;
  }
  
  // 盤面
  Board *board = new Board();

  // AIを設定
  AI *ai = new AI(board, isBlack);
  ai->SetParameter(12, 12, 12);
  ai->LoadPattern();

  // ゲーム開始
  bool gameover = false;
  while(!gameover) {
    short x, y;
    //
    // AIの手番
    //
    if (board->isPass()) {
      // パス
      board->ProgressGame();
      sock->PutDisc(-1, -1, &gameover);
      std::cout << "AI: Pass m(ﾟ台ﾟ)m" << std::endl;
    } else {
      // 手番
      if (board->isBlackNow() == isBlack) {
	// 考える
	if (ai->Ponder(x, y)) {
	  board->PrintBoard();
	  // 石を置く
	  std::cout << "AI: (" << x << ", " << y << ")" << std::endl;
	  board->PutDisc(board->MakeMask(y, x));
	  // データを送る
	  sock->PutDisc(x, y, &gameover);
	} else {
	  std::cout << "[ERROR] AI quit his job!" << std::endl;
	}
      }
    }
    if (gameover) break;

    //
    // 相手の手番
    //
    sock->GetDisc(x, y, &gameover);
    if (x == -1 || y == -1) {
      board->ProgressGame();
      std::cout << "Opponent: Pass m(ﾟ台ﾟ)m" << std::endl;
    } else {
      board->PrintBoard();
      // 石を置く
      std::cout << "Opponent: (" << x << ", " << y << ")" << std::endl;
      board->PutDisc(board->MakeMask(y, x));
    }
  }
  
  std::cout << "----- GAME OVER -----" << std::endl;
  board->PrintBoard();

  return 0;
}

/*
 *
 * デバッグモード
 *   -- AI vs 人間 --
 *
 */
int debug(void)
{
  bool isBlack = false;

  // 盤面
  Board *board = new Board();

  // AIを設定
  AI *ai = new AI(board, isBlack);
  ai->SetParameter(12, 12, 12);
  ai->LoadPattern();

  // ゲーム開始
  while(true) {
    short x, y;
    //
    // AIの手番
    //
    if (board->isPass()) {
      // パス
      board->ProgressGame();
      std::cout << "AI: Pass m(ﾟ台ﾟ)m" << std::endl;
    } else {
      // 考える
      if (ai->Ponder(x, y)) {
	board->PrintBoard();
	// 石を置く
	std::cout << "AI: (" << x << ", " << y << ")" << std::endl;
	board->PutDisc(board->MakeMask(y, x));
      }
    }
    //
    // 人間の手番(debug)
    //
    if (board->isPass()) {
      board->ProgressGame();
      std::cout << "You: Pass m(ﾟ台ﾟ)m" << std::endl;
    } else {
      board->PrintBoard();
      // 入力
      std::string move;
      while(true) {
	std::cout << "Your move: ";
	std::cin >> move;
	if (move.size() == 2) {
	  x = (int)(move[0] - 'A');
	  y = (int)(move[1] - '1');
	}
	if ((x >= 0) && (x < 8) && (y >= 0) && (y < 8)) {
	  if (board->canMove(board->MakeMask(y, x))) {
	    break;
	  } else {
	    std::cout << "You cannot put a disc there." << std::endl;
	  }
	}
      }
      // 石を置く
      std::cout << "You: (" << x << ", " << y << ")" << std::endl;
      board->PutDisc(board->MakeMask(y, x));
    }

    // 終了か
    if (board->isGameOver()) {
      board->PrintBoard();
      std::cout << "----- GAME OVER -----" << std::endl;
      break;
    }
  }

  return 0;
}
