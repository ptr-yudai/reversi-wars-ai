/*
 * AIの本体
 */
#include "ai.hpp"

/*
 * コンストラクタ
 */
AI::AI(Board *board, bool isBlack)
{
  this->board = board;
  this->isBlack = isBlack;
  this->pattern_file = "pattern.ptr";

  this->SetParameter(1, 1, 1); // 最弱
}

/*
 * AIのパラメータを設定
 */
void AI::SetParameter(short mid_depth, short wld_depth, short ex_depth)
{
  this->mid_depth = mid_depth;
  this->wld_depth = wld_depth;
  this->ex_depth  = ex_depth;
}

/*
 * 石を打つ
 */
bool AI::Ponder(short &x, short &y)
{
  if (this->board->isBlackNow() != this->isBlack) {
    // 相手の手番ではキャンセル
    x = -1;
    y = -1;
    return false;
  }

  int eval;
  BitBoard result = 0UL;

  short left_moves = 60 - this->board->CountDisc();
  BitBoard black = this->board->GetBoard(true);
  BitBoard white = this->board->GetBoard(false);

  if (left_moves <= this->ex_depth) {
    // 完全先読み可能
    eval = this->EndSearch(this->isBlack, black, white, left_moves, -0xFFFF, 0xFFFF, result);
    eval *= DISC_EVAL;
    this->UpdateAllPattern(eval, this->isBlack, black, white);
  } else if (left_moves <= this->wld_depth) {
    // 必勝先読み可能
    eval = this->EndSearch(this->isBlack, black, white, left_moves, -0xFFFF, 0xFFFF, result);
    eval *= DISC_EVAL;
  } else {
    // 通常先読み
    eval = this->EndSearch(this->isBlack, black, white, this->mid_depth, -0xFFFF, 0xFFFF, result);
  }

  if (result == 0) {
    return false;
  } else {
    // 座標に戻す
    int i;
    for(i = 0; i < 64; i++) {
      if ((result & 1) == 1) break;
      result = result >> 1;
    }
    x = i % 8;
    y = i / 8;
    std::cout << "AI: eval = " << eval << std::endl;
  }

  return true;
}

/*
 * 評価関数2 : 石数評価
 */
int AI::EvalDisc(bool next, BitBoard black, BitBoard white)
{
  if (next) {
    return this->CountBit(black) - this->CountBit(white);
  } else {
    return this->CountBit(white) - this->CountBit(black);
  }
}

/*
 * 評価関数1 : パターン評価
 */
int AI::EvalPattern(bool next, BitBoard black, BitBoard white)
{
  int eval = 0;
  
  // DIAG4 : A4, B3, C2, D1
  eval += this->eval_pattern[PATTERN::DIAG4][CalcIndex(next, black, white, 24, 17, 10, 3)];
  // DIAG4 : H4, G3, F2, E1
  eval += this->eval_pattern[PATTERN::DIAG4][CalcIndex(next, black, white, 31, 22, 13, 4)];
  // DIAG4 : A5, B6, C7, D8
  eval += this->eval_pattern[PATTERN::DIAG4][CalcIndex(next, black, white, 32, 41, 50, 59)];
  // DIAG4 : H5, G6, F7, E8
  eval += this->eval_pattern[PATTERN::DIAG4][CalcIndex(next, black, white, 39, 46, 53, 60)];

  // DIAG5 : A5, B4, C3, D2, E1
  eval += this->eval_pattern[PATTERN::DIAG5][CalcIndex(next, black, white, 32, 25, 18, 11, 4)];
  // DIAG5 : H5, G4, F3, E2, D1
  eval += this->eval_pattern[PATTERN::DIAG5][CalcIndex(next, black, white, 39, 30, 32, 12, 3)];
  // DIAG5 : A4, B5, C6, D7, E8
  eval += this->eval_pattern[PATTERN::DIAG5][CalcIndex(next, black, white, 24, 33, 42, 51, 60)];
  // DIAG5 : H4, G5, F6, E7, D8
  eval += this->eval_pattern[PATTERN::DIAG5][CalcIndex(next, black, white, 31, 38, 45, 52, 59)];
  
  // DIAG6 : A6, B5, C4, D3, E2, F1
  eval += this->eval_pattern[PATTERN::DIAG6][CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5)];
  // DIAG6 : H6, G5, F4, E3, D2, C1
  eval += this->eval_pattern[PATTERN::DIAG6][CalcIndex(next, black, white, 47, 38, 29, 20, 11, 2)];
  // DIAG6 : A3, B4, C5, D6, E7, F8
  eval += this->eval_pattern[PATTERN::DIAG6][CalcIndex(next, black, white, 16, 25, 34, 43, 52, 61)];
  // DIAG6 : H3, G4, F5, E6, D7, C8
  eval += this->eval_pattern[PATTERN::DIAG6][CalcIndex(next, black, white, 23, 30, 37, 44, 51, 58)];

  // DIAG7 : A7, B6, C5, D4, E3, F2, G1
  eval += this->eval_pattern[PATTERN::DIAG7][CalcIndex(next, black, white, 48, 41, 34, 27, 20, 13, 6)];
  // DIAG7 : H7, G6, F5, E4, D3, C2, B1
  eval += this->eval_pattern[PATTERN::DIAG7][CalcIndex(next, black, white, 55, 46, 37, 28, 19, 10, 1)];
  // DIAG7 : A2, B3, C4, D5, E6, F7, G8
  eval += this->eval_pattern[PATTERN::DIAG7][CalcIndex(next, black, white, 8, 17, 26, 35, 44, 53, 62)];
  // DIAG7 : H2, G3, F4, E5, D6, C7, B8
  eval += this->eval_pattern[PATTERN::DIAG7][CalcIndex(next, black, white, 15, 22, 29, 36, 43, 50, 57)];

  // ここでエラー
  std::cout << "over" << std::endl;
  // EDGE2X : A1, B1, C1, D1, E1, F1, G1, H1, B2, G2
  eval += this->eval_pattern[PATTERN::EDGE2X][CalcIndex(next, black, white, 0, 1, 2, 3, 4, 5, 6, 7, 9, 14)];
  // EDGE2X : A8, B8, C8, D8, E8, F8, G8, H8, B7, G7
  eval += this->eval_pattern[PATTERN::EDGE2X][CalcIndex(next, black, white, 56, 57, 58, 59, 60, 61, 62, 63, 49, 54)];
  // EDGE2X : A1, A2, A3, A4, A5, A6, A7, A8, B2, B7
  eval += this->eval_pattern[PATTERN::EDGE2X][CalcIndex(next, black, white, 0, 8, 16, 24, 32, 40, 48, 5, 9, 49)];
  // EDGE2X : H1, H2, H3, H4, H5, H6, H7, H8, G2, G7
  eval += this->eval_pattern[PATTERN::EDGE2X][CalcIndex(next, black, white, 7, 15, 23, 31, 39, 47, 55, 63, 14, 54)];
  std::cout << "over!" << std::endl;

  // HV2 : A2, B2, C2, D2, E2, F2, G2, H2
  eval += this->eval_pattern[PATTERN::HV2][CalcIndex(next, black, white, 8, 9, 10, 11, 12, 13, 14, 15)];
  // HV2 : A7, B7, C7, D7, E7, F7, G7, H7
  eval += this->eval_pattern[PATTERN::HV2][CalcIndex(next, black, white, 48, 49, 50, 51, 52, 53, 54, 55)];
  // HV2 : B1, B2, B3, B4, B5, B6, B7, B8
  eval += this->eval_pattern[PATTERN::HV2][CalcIndex(next, black, white, 1, 9, 17, 25, 33, 41, 49, 57)];
  // HV2 : G1, G2, G3, G4, G5, G6, G7, G8
  eval += this->eval_pattern[PATTERN::HV2][CalcIndex(next, black, white, 6, 14, 22, 30, 38, 46, 54, 62)];

  // HV3 : A3, B3, C3, D3, E3, F3, G3, H3
  eval += this->eval_pattern[PATTERN::HV3][CalcIndex(next, black, white, 16, 17, 18, 19, 20, 21, 22, 23)];
  // HV3 : A6, B6, C6, D6, E6, F6, G6, H6
  eval += this->eval_pattern[PATTERN::HV3][CalcIndex(next, black, white, 40, 41, 42, 43, 44, 45, 46, 47)];
  // HV3 : C1, C2, C3, C4, C5, C6, C7, C8
  eval += this->eval_pattern[PATTERN::HV3][CalcIndex(next, black, white, 2, 10, 18, 26, 34, 42, 50, 58)];
  // HV3 : F1, F2, F3, F4, F5, F6, F7, F8
  eval += this->eval_pattern[PATTERN::HV3][CalcIndex(next, black, white, 5, 13, 21, 29, 37, 45, 53, 61)];

  // HV4 : A4, B4, C4, D4, E4, F4, G4, H4
  eval += this->eval_pattern[PATTERN::HV4][CalcIndex(next, black, white, 24, 25, 26, 27, 28, 29, 30, 31)];
  // HV4 : A5, B5, C5, D5, E5, F5, G5, H5
  eval += this->eval_pattern[PATTERN::HV4][CalcIndex(next, black, white, 32, 33, 34, 35, 36, 37, 38, 39)];
  // HV4 : D1, D2, D3, D4, D5, D6, D7, D8
  eval += this->eval_pattern[PATTERN::HV4][CalcIndex(next, black, white, 3, 11, 19, 27, 35, 43, 51, 59)];
  // HV4 : E1, E2, E3, E4, E5, E6, E7, E8
  eval += this->eval_pattern[PATTERN::HV4][CalcIndex(next, black, white, 4, 12, 20, 28, 36, 44, 52, 60)];
  
  // CORNER25: A1, B1, C1, D1, E1, A2, B2, C2, D2, E2
  eval += this->eval_pattern[PATTERN::CORNER25][CalcIndex(next, black, white, 0, 1, 2, 3, 4, 8, 9, 10, 11, 12)];
  // CORNER25: A7, B7, C7, D7, E7, A8, B8, C8, D8, E8
  eval += this->eval_pattern[PATTERN::CORNER25][CalcIndex(next, black, white, 48, 49, 50, 51, 52, 56, 57, 58, 59, 60)];
  // CORNER25: D1, E1, F1, G1, H1, D2, E2, F2, G2, H2
  eval += this->eval_pattern[PATTERN::CORNER25][CalcIndex(next, black, white, 3, 4, 5, 6, 7, 11, 12, 13, 14, 15)];
  // CORNER25: D7, E7, F7, G7, H7, D8, E8, F8, G8, H8
  eval += this->eval_pattern[PATTERN::CORNER25][CalcIndex(next, black, white, 51, 52, 53, 54, 55, 59, 60, 61, 62, 63)];
  // CORNER33: A1, B1, C1, A2, B2, C2, A3, B3, C3
  eval += this->eval_pattern[PATTERN::CORNER33][CalcIndex(next, black, white, 0, 1, 2, 8, 9, 10, 16, 17, 18)];
  // CORNER33: A6, B6, C6, A7, B7, C7, A8, B8, C8
  eval += this->eval_pattern[PATTERN::CORNER33][CalcIndex(next, black, white, 40, 41, 42, 48, 49, 50, 56, 57, 58)];
  // CORNER33: F1, G1, H1, F2, G2, H2, F3, G3, H3
  eval += this->eval_pattern[PATTERN::CORNER33][CalcIndex(next, black, white, 5, 6, 7, 13, 14, 15, 21, 22, 23)];
  // CORNER33: F6, G6, H6, F7, G7, H7, F8, G8, H8
  eval += this->eval_pattern[PATTERN::CORNER33][CalcIndex(next, black, white, 45, 46, 47, 53, 54, 55, 61, 62, 63)];
  
  return eval;
}

/*
 * 評価値の更新
 */
void AI::UpdateAllPattern(int eval, bool next, BitBoard black, BitBoard white)
{
  int index, diff;
  diff = (int)((eval - this->EvalPattern(next, black, white)) * ALPHA_UPDATE);

  // DIAG4 : A4, B3, C2, D1
  index = CalcIndex(next, black, white, 24, 17, 10, 3);
  this->UpdatePattern(PATTERN::DIAG4, this->mirror_line[index], index, diff);
  // DIAG4 : H4, G3, F2, E1
  index = CalcIndex(next, black, white, 31, 22, 13, 4);
  this->UpdatePattern(PATTERN::DIAG4, this->mirror_line[index], index, diff);
  // DIAG4 : A5, B6, C7, D8
  index = CalcIndex(next, black, white, 32, 41, 50, 59);
  this->UpdatePattern(PATTERN::DIAG4, this->mirror_line[index], index, diff);
  // DIAG4 : H5, G6, F7, E8
  index = CalcIndex(next, black, white, 39, 46, 53, 60);
  this->UpdatePattern(PATTERN::DIAG4, this->mirror_line[index], index, diff);
  
  // DIAG5 : A5, B4, C3, D2, E1
  index = CalcIndex(next, black, white, 32, 25, 18, 11, 4);
  this->UpdatePattern(PATTERN::DIAG5, this->mirror_line[index], index, diff);
  // DIAG5 : H5, G4, F3, E2, D1
  index = CalcIndex(next, black, white, 32, 25, 18, 11, 4);
  this->UpdatePattern(PATTERN::DIAG5, this->mirror_line[index], index, diff);
  // DIAG5 : A4, B5, C6, D7, E8
  index = CalcIndex(next, black, white, 32, 25, 18, 11, 4);
  this->UpdatePattern(PATTERN::DIAG5, this->mirror_line[index], index, diff);
  // DIAG5 : H4, G5, F6, E7, D8
  index = CalcIndex(next, black, white, 32, 25, 18, 11, 4);
  this->UpdatePattern(PATTERN::DIAG5, this->mirror_line[index], index, diff);
  
  // DIAG6 : A6, B5, C4, D3, E2, F1
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::DIAG6, this->mirror_line[index], index, diff);
  // DIAG6 : H6, G5, F4, E3, D2, C1
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::DIAG6, this->mirror_line[index], index, diff);
  // DIAG6 : A3, B4, C5, D6, E7, F8
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::DIAG6, this->mirror_line[index], index, diff);
  // DIAG6 : H3, G4, F5, E6, D7, C8
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::DIAG6, this->mirror_line[index], index, diff);

  // DIAG7 : A7, B6, C5, D4, E3, F2, G1
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::DIAG7, this->mirror_line[index], index, diff);
  // DIAG7 : H7, G6, F5, E4, D3, C2, B1
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::DIAG7, this->mirror_line[index], index, diff);
  // DIAG7 : A2, B3, C4, D5, E6, F7, G8
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::DIAG7, this->mirror_line[index], index, diff);
  // DIAG7 : H2, G3, F4, E5, D6, C7, B8
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::DIAG7, this->mirror_line[index], index, diff);

  // EDGE2X : A1, B1, C1, D1, E1, F1, G1, H1, B2, G2
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::EDGE2X, index, -1, diff);
  // EDGE2X : A8, B8, C8, D8, E8, F8, G8, H8, B7, G7
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::EDGE2X, index, -1, diff);
  // EDGE2X : A1, A2, A3, A4, A5, A6, A7, A8, B2, B7
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::EDGE2X, index, -1, diff);
  // EDGE2X : H1, H2, H3, H4, H5, H6, H7, H8, G2, G7
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::EDGE2X, index, -1, diff);

  // HV2 : A2, B2, C2, D2, E2, F2, G2, H2
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::HV2, this->mirror_line[index], index, diff);
  // HV2 : A7, B7, C7, D7, E7, F7, G7, H7
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::HV2, this->mirror_line[index], index, diff);
  // HV2 : B1, B2, B3, B4, B5, B6, B7, B8
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::HV2, this->mirror_line[index], index, diff);
  // HV2 : G1, G2, G3, G4, G5, G6, G7, G8
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::HV2, this->mirror_line[index], index, diff);

  // HV3 : A3, B3, C3, D3, E3, F3, G3, H3
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::HV3, this->mirror_line[index], index, diff);
  // HV3 : A6, B6, C6, D6, E6, F6, G6, H6
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::HV3, this->mirror_line[index], index, diff);
  // HV3 : C1, C2, C3, C4, C5, C6, C7, C8
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::HV3, this->mirror_line[index], index, diff);
  // HV3 : F1, F2, F3, F4, F5, F6, F7, F8
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::HV3, this->mirror_line[index], index, diff);

  // HV4 : A4, B4, C4, D4, E4, F4, G4, H4
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::HV4, this->mirror_line[index], index, diff);
  // HV4 : A5, B5, C5, D5, E5, F5, G5, H5
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::HV4, this->mirror_line[index], index, diff);
  // HV4 : D1, D2, D3, D4, D5, D6, D7, D8
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::HV4, this->mirror_line[index], index, diff);
  // HV4 : E1, E2, E3, E4, E5, E6, E7, E8
  index = CalcIndex(next, black, white, 40, 33, 26, 19, 12, 5);
  this->UpdatePattern(PATTERN::HV4, this->mirror_line[index], index, diff);

  // CORNER25: A1, B1, C1, D1, E1, A2, B2, C2, D2, E2
  index = CalcIndex(next, black, white, 0, 1, 2, 3, 4, 8, 9, 10, 11, 12);
  this->UpdatePattern(PATTERN::CORNER25, this->mirror_line[index], index, diff);
  // CORNER25: A7, B7, C7, D7, E7, A8, B8, C8, D8, E8
  index = CalcIndex(next, black, white, 0, 1, 2, 3, 4, 8, 9, 10, 11, 12);
  this->UpdatePattern(PATTERN::CORNER25, this->mirror_line[index], index, diff);
  // CORNER25: D1, E1, F1, G1, H1, D2, E2, F2, G2, H2
  index = CalcIndex(next, black, white, 0, 1, 2, 3, 4, 8, 9, 10, 11, 12);
  this->UpdatePattern(PATTERN::CORNER25, this->mirror_line[index], index, diff);
  // CORNER25: D7, E7, F7, G7, H7, D8, E8, F8, G8, H8
  index = CalcIndex(next, black, white, 0, 1, 2, 3, 4, 8, 9, 10, 11, 12);
  this->UpdatePattern(PATTERN::CORNER25, this->mirror_line[index], index, diff);

  // CORNER33: A1, B1, C1, A2, B2, C2, A3, B3, C3
  index = CalcIndex(next, black, white, 0, 1, 2, 3, 4, 8, 9, 10, 11, 12);
  this->UpdatePattern(PATTERN::CORNER33, this->mirror_line[index], index, diff);
  // CORNER33: A6, B6, C6, A7, B7, C7, A8, B8, C8
  index = CalcIndex(next, black, white, 0, 1, 2, 3, 4, 8, 9, 10, 11, 12);
  this->UpdatePattern(PATTERN::CORNER33, this->mirror_line[index], index, diff);
  // CORNER33: F1, G1, H1, F2, G2, H2, F3, G3, H3
  index = CalcIndex(next, black, white, 0, 1, 2, 3, 4, 8, 9, 10, 11, 12);
  this->UpdatePattern(PATTERN::CORNER33, this->mirror_line[index], index, diff);
  // CORNER33: F6, G6, H6, F7, G7, H7, F8, G8, H8
  index = CalcIndex(next, black, white, 0, 1, 2, 3, 4, 8, 9, 10, 11, 12);
  this->UpdatePattern(PATTERN::CORNER33, this->mirror_line[index], index, diff);

  this->SavePattern();
}

/*
 * パターンを更新
 */
void AI::UpdatePattern(int pattern, int index, int mirror, int diff)
{
  if (MAX_PATTERN_EVAL - diff < this->eval_pattern[pattern][index]) {
    // 正に最大
    this->eval_pattern[pattern][index] = MAX_PATTERN_EVAL;
  } else if (-MAX_PATTERN_EVAL - diff > this->eval_pattern[pattern][index]) {
    // 負に最大
    this->eval_pattern[pattern][index] = -MAX_PATTERN_EVAL;
  } else {
    // 偏差
    this->eval_pattern[pattern][index] += diff;
  }

  // 反対のパターンも更新
  if (mirror >= 0) {
    this->eval_pattern[pattern][mirror] = this->eval_pattern[pattern][index];
  }
}

/*
 * パターン用のインデックスを計算
 */
template<class... T>
int AI::CalcIndex(bool next, BitBoard black, BitBoard white, T... args)
{
  int result = 0;
  
  // 各マスの状態からインデックスを計算
  for(int w : std::initializer_list<int>{args...}) {
    if ((black >> w) & 1) {
      result += 1;
    } else if ((white >> w) & 1) {
      result += 2;
    }
    result *= 3;
  }

  return result;
}

/*
 * n手先の局面まで探索する
 *
 * next       : 次に打つ人が黒か
 * black      : 黒のビットボード
 * white      : 白のビットボード
 * left_moves : 探索終了までの残り手数
 * alpha      : アルファ
 * beta       : ベータ
 */
int AI::EndSearch(bool next, BitBoard black, BitBoard white, short left_moves, int alpha, int beta, BitBoard &res_move)
{
  int eval;
  int max = alpha;
  BitBoard move_tmp;

  if (left_moves == 0) {
    // 最終局面なら評価値を返す
    // return this->EvalDisc(next, black, white);
    return this->EvalPattern(next, black, white);
  }

  // 着手可能なボード
  BitBoard legalBoard = this->board->GetLegalBoard(next, black, white);

  if (legalBoard == 0UL) {
    // 着手できない
    if (this->board->GetLegalBoard(!next, black, white) == 0) {
      // 相手も着手できなければ終了
      if (next) {
	return CountBit(black) - CountBit(white);
      } else {
	return CountBit(white) - CountBit(black);
      }
    } else {
      // 相手は着手できる
      return -this->EndSearch(!next, black, white, left_moves, -beta, -max, move_tmp);
    }
  }

  // 探索
  bool flag_move = true;
  while(legalBoard) {
    // 最左の1だけを残したボードを取り出す
    BitBoard move = legalBoard & (-legalBoard);
    // 最左の1を0にする
    legalBoard &= (legalBoard - 1);
    // 着手可能ならマスクを取得
    BitBoard mask_flip = this->board->MakeFlipMask(next, move, black, white);
    
    // 探索の端は思考
    if (flag_move) {
      res_move = move;
      flag_move = false;
    }

    // ビットボードを反転して探索
    if (next) {
      eval = -this->EndSearch(!next,
			      black ^ (move | mask_flip),
			      white ^ mask_flip,
			      left_moves - 1,
			      -beta, -max,
			      move_tmp);
    } else {
      eval = -this->EndSearch(!next,
			      black ^ mask_flip,
			      white ^ (move | mask_flip),
			      left_moves - 1,
			      -beta, -max,
			      move_tmp);
    }
    
    if (eval > max) {
      max = eval;
      res_move = move;
      // ベータよりええやん
      if (max >= beta) {
	return beta;
      }
    }
  }
  
  return max;
}

/*
 * 1が立っているビット数を数える
 */
int AI::CountBit(BitBoard board)
{
  board = (board & 0x5555555555555555) + (board >> 1  & 0x5555555555555555);
  board = (board & 0x3333333333333333) + (board >> 2  & 0x3333333333333333);
  board = (board & 0x0F0F0F0F0F0F0F0F) + (board >> 4  & 0x0F0F0F0F0F0F0F0F);
  board = (board & 0x00FF00FF00FF00FF) + (board >> 8  & 0x00FF00FF00FF00FF);
  board = (board & 0x0000FFFF0000FFFF) + (board >> 16 & 0x0000FFFF0000FFFF);
  return  (board & 0x00000000FFFFFFFF) + (board >> 32 & 0x00000000FFFFFFFF);
}

/*
 * パターンファイルをロード
 */
void AI::LoadPattern()
{
  int coeff[] = {9, 243, 1, 27, 729, 3, 81, 2187};

  // メモリ初期化
  for(int i = 0; i < PATTERN::END_OF_PATTERN; i++) {
    this->eval_pattern[i] = new int[this->PATTERN_SIZE[i]];
  }
  // 反対マスの計算
  for(int i = 0; i < 6561; i++) {
    int mirror_i = i;
    int mirror_o = 0;
    int coeff_ = 2187;
    for(int j = 0; j < 8; j++) {
      mirror_o += mirror_i % 3 * coeff_;
      mirror_i /= 3;
      coeff_ /= 3;
    }
    if (mirror_o < i) {
      this->mirror_line[i] = mirror_o;
    } else {
      this->mirror_line[i] = i;
    }
  }
  // 対称マスの計算
  for(int i = 0; i < 6561; i++) {
    int mirror_i = i;
    int mirror_o = 0;
    for(int j = 0; j < 8; j++) {
      mirror_o += mirror_i % 3 * coeff[j];
      mirror_i /= 3;
    }
    if (mirror_o < i) {
      this->mirror_corner[i] = mirror_o;
    } else {
      this->mirror_corner[i] = i;
    }
  }

  // ファイル読み込み  
  std::ifstream ifs(this->pattern_file);
  if (ifs.fail()) {
    std::cerr << "File does not exist." << std::endl;
    return;
  }

  for(int i = 0; i < PATTERN::END_OF_PATTERN; i++) {
    std::string str;
    std::getline(ifs, str);
    for(int j = 0; j < this->PATTERN_SIZE[i]; j++) {
      std::sscanf(str.data(), "%d", &this->eval_pattern[i][j]);
    }
  }

  std::cout << "[INFO] Pattern: " << this->pattern_file << std::endl;
}

/*
 * パターンファイルをセーブ
 */
void AI::SavePattern()
{
  std::ofstream ofs(this->pattern_file);
  std::cout << "Saving..." << std::endl;

  for(int i = 0; i < PATTERN::END_OF_PATTERN; i++) {
    for(int j = 0; j < this->PATTERN_SIZE[i]; j++) {
      ofs << this->eval_pattern[i][j] << " ";
    }
    ofs << "\n";
  }

  ofs.close();
}

/*
 * デストラクタ
 */
AI::~AI()
{
  this->SavePattern();
}
