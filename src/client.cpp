/*
 * 通信クライアント
 */
#include "client.hpp"

/*
 * コンストラクタ
 */
Client::Client(std::string address, int port)
{
  this->sock = -1;
  this->address = address;
  this->port = port;
}

/*
 * 接続要求
 */
bool Client::Connect()
{
  // ソケット生成
  if (this->sock == -1) {
    this->sock = socket(AF_INET, SOCK_STREAM, 0);
    if (this->sock == -1) {
      std::cerr << "[ERROR] Could not create socket" << std::endl;
    }
  }
  // アドレス構造体の設定
  if (inet_addr(this->address.c_str()) == INADDR_NONE) {
    struct hostent *he;
    struct in_addr **addr_list;
    // ホスト名を解決
    if ((he = gethostbyname(this->address.c_str())) == NULL) {
      std::cerr << "[ERROR] Could not resolve hostname" << std::endl;
      return false;
    }
    // IPアドレスに変換
    addr_list = (struct in_addr **)he->h_addr_list;
    for(int i = 0; addr_list[i] != NULL; i++) {
      this->server.sin_addr = *addr_list[i];
      std::cout << this->address << " --> " << inet_ntoa(*addr_list[i]) << std::endl;
      break;
    }
  } else {
    // IPアドレスならそのまま
    this->server.sin_addr.s_addr = inet_addr(this->address.c_str());
  }

  this->server.sin_family = AF_INET;
  this->server.sin_port = htons(port);

  // サーバーに接続
  if (connect(this->sock, (struct sockaddr *)&this->server, sizeof(this->server)) < 0) {
    std::cerr << "[ERROR] Could not connect to remote server." << std::endl;
    return false;
  }

  // 最初のデータを取得
  std::string reply = this->Receive(512);
  picojson::value json_value;
  std::string err = picojson::parse(json_value, reply);
  if (!err.empty()) {
    std::cerr << "[ERROR] Invalid JSON Value" << std::endl
	      << err << std::endl
	      << "Reply = " << reply << std::endl;
    return false;
  } else {
    std::cout << "!result = "
	      << this->GetResult(json_value)
	      << std::endl;
  }

  return true;
}

/*
 * ログイン
 */
bool Client::Login(std::string username, std::string password)
{
  // ログイン要求
  std::string command;
  command  = "{\"action\"  : \"login\",";
  command += " \"userinfo\": {";
  command += " \"username\": \"" + username + "\",";
  command += " \"password\": \"" + password + "\"}}";
  this->Send(command);
  
  // 結果取得
  std::string reply = this->Receive(512);
  picojson::value json_value;
  std::string err = picojson::parse(json_value, reply);
  if (!err.empty()) {
    std::cerr << "[ERROR] Invalid JSON Value" << std::endl
	      << err << std::endl
	      << "Reply = " << reply << std::endl;
    return false;
  }

  // ログイン失敗
  if (!this->GetResult(json_value)) {
    std::string msg = json_value.get<picojson::object>()["msg"].get<std::string>();
    std::cerr << "[ERROR] Error while login" << std::endl
	      << "!msg = " << msg << std::endl;
    return false;
  }
  
  // ユーザーリスト取得
  picojson::array &userlist = json_value.get<picojson::object>()["users"].get<picojson::array>();
  std::cout << "----- Opponents -----" << std::endl;
  for(picojson::array::iterator it = userlist.begin(), eit = userlist.end(); it != eit; ++it) {
    std::string name = it->get<picojson::object>()["name"].get<std::string>();
    double rating = it->get<picojson::object>()["rating"].get<double>();
    std::cout << "+ " << name << "(" << rating << ")" << std::endl;
  }
  std::cout << "---------------------" << std::endl;
  return true;
}

/*
 * 対戦および待ち要求
 */
bool Client::Match(std::string username, bool isStandby, bool *isBlack)
{
  std::string command = "{\"action\":";

  if (isStandby) {
    command += "\"wait\"}";
  } else {
    command += "\"battle\",";
    command += "\"user\":\"" + username + "\"}";
  }
  // 要求
  this->Send(command);

  // 応答待ち
  std::string reply = this->Receive(512);
  picojson::value json_value;
  std::string err = picojson::parse(json_value, reply);
  if (!err.empty()) {
    std::cerr << "[ERROR] Invalid JSON Value" << std::endl
	      << err << std::endl
	      << "Reply = " << reply << std::endl;
    return false;
  }
  
  // 対戦失敗
  if (!this->GetResult(json_value)) {
    std::cerr << "[ERROR] No match" << std::endl;
    return false;
  }
  
  // 詳細取得
  std::string mark = json_value.get<picojson::object>()["mark"].get<std::string>();
  *isBlack = (mark.compare("black") == 0);
  double id = json_value.get<picojson::object>()["id"].get<double>();
  std::cout << "***** [ID: " << id << "] *****" << std::endl;
  std::cout << "# Your disc is " << mark << std::endl;

  return true;
}

/*
 * 石を置く
 */
bool Client::PutDisc(short x, short y, bool* isGameEnd)
{
  std::string command = "{\"action\":";

  // 行動を送信
  if (x == -1 || y == -1) {
    command += "\"pass\"}";
  } else {
    command += "\"put\",\"pos\":";
    command += "[" + std::to_string(x) + "," + std::to_string(y) + "]}";
  }
  this->Send(command);

  // 結果を取得
  std::string reply = this->Receive(512);
  picojson::value json_value;
  std::string err = picojson::parse(json_value, reply);
  if (!err.empty()) {
    std::cerr << "[ERROR] Invalid JSON Value" << std::endl
	      << err << std::endl
	      << "Reply = " << reply << std::endl;
    return false;
  }
  
  // なにか失敗
  if (!this->GetResult(json_value)) {
    std::cerr << "[ERROR] Invalid request?" << std::endl;
    return false;
  }
  
  // 詳細取得
  std::string gameover = json_value.get<picojson::object>()["isGameEnd"].get<std::string>();
  std::cout << "!isGameEnd = " << gameover << std::endl;
  
  if (gameover.compare("true") == 0) {
    *isGameEnd = true;
    std::cout << "!youWin = "
	      << json_value.get<picojson::object>()["youWin"].get<std::string>()
	      << std::endl;
    std::cout << "!isDraw = "
	      << json_value.get<picojson::object>()["isDraw"].get<std::string>()
	      << std::endl;
  }

  return true;
}

/*
 * 相手の石を取得する
 */
bool Client::GetDisc(short& x, short& y, bool *isGameEnd)
{
  // 結果を取得
  std::string reply = this->Receive(512);
  picojson::value json_value;
  std::string err = picojson::parse(json_value, reply);
  if (!err.empty()) {
    std::cerr << "[ERROR] Invalid JSON Value" << std::endl
	      << err << std::endl
	      << "Reply = " << reply << std::endl;
    return false;
  }
  
  // なにか失敗
  if (!this->GetResult(json_value)) {
    std::cerr << "[ERROR] Invalid request?" << std::endl;
    return false;
  }
  
  // 行動取得
  std::string action = json_value.get<picojson::object>()["action"].get<std::string>();
  
  // パス
  if (action.compare("pass") == 0) {
    x = -1; y = -1;
    return true;
  }
  
  // 石の場所を取得
  if (action.compare("put") == 0) {
    picojson::array &pos = json_value.get<picojson::object>()["pos"].get<picojson::array>();
    x = pos[0].get<double>();
    y = pos[1].get<double>();
    
    // ゲーム終了か
    std::string gameover = json_value.get<picojson::object>()["isGameEnd"].get<std::string>();
    if (gameover.compare("true") == 0) {
      *isGameEnd = true;
      std::cout << "!youWin = "
		<< json_value.get<picojson::object>()["youWin"].get<std::string>()
		<< std::endl;
      std::cout << "!isDraw = "
		<< json_value.get<picojson::object>()["isDraw"].get<std::string>()
		<< std::endl;
    }

    return true;
  }

  return false;
}

/*
 * JSONからresultを取り出す
 */
bool Client::GetResult(picojson::value json)
{
  std::string result = json.get<picojson::object>()["result"].get<std::string>();
  return (result.compare("true") == 0);
}

/*
 * データを取得
 */
std::string Client::Receive(int size = 1024)
{
  char buffer[size];
  std::string reply;

  std::memset(buffer, 0, sizeof(char) * size);
  if (recv(this->sock, buffer, sizeof(buffer), 0) < 0) {
    std::cerr << "No reply from server" << std::endl;
  }

  reply = buffer;
  return reply;
}

/*
 * データを送信
 */
bool Client::Send(std::string data)
{
  if (send(this->sock, data.c_str(), data.size(), 0) < 0) {
    std::cerr << "Could not send data" << std::endl;
    return false;
  }

  return true;
}
