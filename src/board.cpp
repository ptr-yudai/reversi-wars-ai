/*
 * 盤面クラスの本体
 */
#include "board.hpp"

/*
 * コンストラクタ
 */
Board::Board()
{
  this->Initialize();
}

/*
 * 初期化
 */
void Board::Initialize()
{
  this->black = 0UL;
  this->white = 0UL;
  // 手数
  this->n_move = 0;
  // 初期状態
  this->black |= this->MakeMask(3, 4);
  this->black |= this->MakeMask(4, 3);
  this->white |= this->MakeMask(3, 3);
  this->white |= this->MakeMask(4, 4);
  // クロから
  this->nextMove = true;
}

/*
 * ボードを取得
 */
BitBoard Board::GetBoard(bool isBlack)
{
  return isBlack ? this->black : this->white;
}

/*
 * 現在の手番
 */
bool Board::isBlackNow()
{
  return this->nextMove;
}

/*
 * 現在の手数
 */
short Board::CountDisc()
{
  return this->n_move;
}

/*
 * 石を置けるか
 */
bool Board::canMove(BitBoard move)
{
  BitBoard legalBoard = this->GetLegalBoard(this->nextMove);
  if ((move & legalBoard) == move) {
    return true;
  }
  return false;
}

/*
 * 着手後の反転マスクを返す
 */
BitBoard Board::GetLegalBoard(bool next, BitBoard black, BitBoard white)
{
  // 盤面
  BitBoard opponent, player;
  if (black == 0 && white == 0) {
    opponent = next ? this->white : this->black;
    player   = next ? this->black : this->white;
  } else {
    opponent = next ? white : black;
    player   = next ? black : white;
  }
  BitBoard blank    = ~(opponent | player);
  // マスク
  BitBoard mask_horizontal = opponent & BB_HORIZONTAL;
  BitBoard mask_vertical   = opponent & BB_VERTICAL;
  BitBoard mask_allside    = opponent & BB_ALLSIDE;

  BitBoard mask;
  BitBoard legalBoard = 0;
  // 左
  mask  = mask_horizontal & (player << 1);
  mask |= mask_horizontal & (mask << 1);
  mask |= mask_horizontal & (mask << 1);
  mask |= mask_horizontal & (mask << 1);
  mask |= mask_horizontal & (mask << 1);
  mask |= mask_horizontal & (mask << 1);
  legalBoard |= blank & (mask << 1);
  // 右
  mask  = mask_horizontal & (player >> 1);
  mask |= mask_horizontal & (mask >> 1);
  mask |= mask_horizontal & (mask >> 1);
  mask |= mask_horizontal & (mask >> 1);
  mask |= mask_horizontal & (mask >> 1);
  mask |= mask_horizontal & (mask >> 1);
  legalBoard |= blank & (mask >> 1);
  // 上
  mask  = mask_vertical & (player << 8);
  mask |= opponent & (mask << 8);
  mask |= opponent & (mask << 8);
  mask |= opponent & (mask << 8);
  mask |= opponent & (mask << 8);
  mask |= opponent & (mask << 8);
  legalBoard |= blank & (mask << 8);
  // 下
  mask  = mask_vertical & (player >> 8);
  mask |= opponent & (mask >> 8);
  mask |= opponent & (mask >> 8);
  mask |= opponent & (mask >> 8);
  mask |= opponent & (mask >> 8);
  mask |= opponent & (mask >> 8);
  legalBoard |= blank & (mask >> 8);
  // 右斜め上
  mask  = mask_allside & (player << 7);
  mask |= mask_horizontal & (mask << 7);
  mask |= mask_horizontal & (mask << 7);
  mask |= mask_horizontal & (mask << 7);
  mask |= mask_horizontal & (mask << 7);
  mask |= mask_horizontal & (mask << 7);
  legalBoard |= blank & (mask << 7);
  // 左斜め上
  mask  = mask_allside & (player << 9);
  mask |= mask_horizontal & (mask << 9);
  mask |= mask_horizontal & (mask << 9);
  mask |= mask_horizontal & (mask << 9);
  mask |= mask_horizontal & (mask << 9);
  mask |= mask_horizontal & (mask << 9);
  legalBoard |= blank & (mask << 9);
  // 右斜め下
  mask  = mask_allside & (player >> 9);
  mask |= mask_horizontal & (mask >> 9);
  mask |= mask_horizontal & (mask >> 9);
  mask |= mask_horizontal & (mask >> 9);
  mask |= mask_horizontal & (mask >> 9);
  mask |= mask_horizontal & (mask >> 9);
  legalBoard |= blank & (mask >> 9);
  // 右斜め上
  mask  = mask_allside & (player >> 7);
  mask |= mask_horizontal & (mask >> 7);
  mask |= mask_horizontal & (mask >> 7);
  mask |= mask_horizontal & (mask >> 7);
  mask |= mask_horizontal & (mask >> 7);
  mask |= mask_horizontal & (mask >> 7);
  legalBoard |= blank & (mask >> 7);

  return legalBoard;
}

/*
 * 石を置く
 */
bool Board::PutDisc(BitBoard move)
{
  BitBoard mask_flip = this->MakeFlipMask(this->nextMove, move);
  
  // 石を反転
  if (this->nextMove) {
    this->black ^= move | mask_flip;
    this->white ^= mask_flip;
  } else {
    this->black ^= mask_flip;
    this->white ^= move | mask_flip;
  }
  
  // 手番を反転
  this->nextMove = !this->nextMove;
  this->n_move++;
  return true;
}

/*
 * 着手時の反転マスクを生成
 */
BitBoard Board::MakeFlipMask(bool next, BitBoard move, BitBoard black, BitBoard white)
{
  BitBoard mask_flip = 0UL;
  BitBoard opponent, player;
  // 盤面
  if (black == 0 && white == 0) {
    opponent = next ? this->white : this->black;
    player   = next ? this->black : this->white;
  } else {
    opponent = next ? white : black;
    player   = next ? black : white;
  }

  // さすがにループ無しはきつい......
  for(int i = 0; i < 8; i++) {
    BitBoard mask_tmp = 0UL;
    BitBoard mask = this->Transfer(move, i);
    // 反転しなくなるまで
    while((mask != 0) && ((mask & opponent) != 0)) {
      mask_tmp |= mask;
      mask = Transfer(mask, i);
    }
    // 反対側に自分の石がある
    if ((mask & player) != 0) {
      mask_flip |= mask_tmp;
    }
  }

  return mask_flip;
}

/*
 * 反転箇所を計算する
 */
BitBoard Board::Transfer(BitBoard move, short n)
{
  switch(n) {
  case 0: // 左
    return (move << 1) & 0xFEFEFEFEFEFEFEFE;
  case 1: // 右
    return (move >> 1) & 0x7F7F7F7F7F7F7F7F;
  case 2: // 上
    return (move << 8) & 0xFFFFFFFFFFFFFF00;
  case 3: // 下
    return (move >> 8) & 0x00FFFFFFFFFFFFFF;
  case 4: // 右上
    return (move << 7) & 0x7F7F7F7F7F7F7F00;
  case 5: // 右下
    return (move >> 9) & 0x007F7F7F7F7F7F7F;
  case 6: // 左上
    return (move << 9) & 0xFEFEFEFEFEFEFE00;
  case 7: // 左下
    return (move >> 7) & 0x00FEFEFEFEFEFEFE;
  default:
    return 0;
  }
}

/*
 * 局面を進める
 */
void Board::ProgressGame()
{
  this->nextMove = !this->nextMove;
}

/*
 * パスか
 */
bool Board::isPass()
{
  return (GetLegalBoard(this->nextMove) == 0);
}

/*
 * ゲーム終了か
 */
bool Board::isGameOver()
{
  return ((GetLegalBoard(true) | GetLegalBoard(false)) == 0);
}

/*
 * ビットボード生成
 */
BitBoard Board::MakeMask(short row, short column)
{
  return (BitBoard)(1UL << (row * 8 + column));
}

/*
 * 特定のマスが黒か
 */
bool Board::isBlack(short row, short column)
{
  return (this->black >> (row * 8 + column)) & 1 ? true : false;
}
bool Board::isBlack(short index)
{
  return (this->black >> index) & 1 ? true : false;
}
/*
 * 特定のマスが白か
 */
bool Board::isWhite(short row, short column)
{
  return (this->white >> (row * 8 + column)) & 1 ? true : false;
}
bool Board::isWhite(short index)
{
  return (this->white >> index) & 1 ? true : false;
}
/*
 * 特定のマスが空か
 */
bool Board::isBlank(short row, short column)
{
  return ((this->white | this->black) >> (row * 8 + column)) & 1 ? false : true;
}
bool Board::isBlank(short index)
{
  return ((this->white | this->black) >> index) & 1 ? false : true;
}

/*
 * 盤面を表示
 */
void Board::PrintBoard(void)
{
  std::cout << "   a b c d e f g h" << std::endl;
  for(int i = 0; i < 64; i++) {
    // 番号
    if (i % 8 == 0) {
      std::cout << (i + 1) / 8 + 1 << " ";
    }
    // 石を表示
    if (this->isBlack(i)) {
      std::cout << "●";
    } else if (this->isWhite(i)) {
      std::cout << "○";
    } else {
      std::cout << "..";
    }
    // 改行
    if (i % 8 == 7) {
      std::cout << std::endl;
    }
  }
}
