/*
 * 通信クライアントの定義
 */
#include "picojson.h"
#include <iostream>
#include <cstring>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>

class Client
{
private:
  int sock;
  std::string address;
  int port;
  struct sockaddr_in server;
public:
  Client(std::string, int);
  bool Connect();
  bool Login(std::string, std::string);
  bool Match(std::string, bool, bool*);
  bool PutDisc(short, short, bool*);
  bool GetDisc(short&, short&, bool*);
  bool GetResult(picojson::value);
  std::string Receive(int);
  bool Send(std::string);
};

