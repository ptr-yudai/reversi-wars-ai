#ifndef BOARD_HPP
#define BOARD_HPP

#include <iostream>
#include "type.hpp"

#define BB_HORIZONTAL 0x7E7E7E7E7E7E7E7E
#define BB_VERTICAL   0x00FFFFFFFFFFFF00
#define BB_ALLSIDE    0x007E7E7E7E7E7E00

/*
 * 盤面クラスの定義
 */
class Board
{
private:
  bool nextMove;
  BitBoard black;
  BitBoard white;
  short n_move;
public:
  Board();
  // 初期化
  void Initialize();
  // 合法手を調べる
  BitBoard GetLegalBoard(bool, BitBoard black = 0, BitBoard white = 0);
  // 反転ビットボードを作成
  BitBoard MakeFlipMask(bool, BitBoard, BitBoard black = 0, BitBoard white = 0);
  // ビットボードを作成
  BitBoard MakeMask(short, short);
  // 石を置けるか
  bool canMove(BitBoard);
  // 石を置く
  bool PutDisc(BitBoard);
  BitBoard Transfer(BitBoard, short);
  // 局面を進める
  void ProgressGame();
  // 局面の情報
  bool isPass();
  bool isGameOver();
  // マス情報
  bool isBlack(short, short);
  bool isBlack(short);
  bool isWhite(short, short);
  bool isWhite(short);
  bool isBlank(short, short);
  bool isBlank(short);
  // 手番情報
  bool isBlackNow();
  // 手数情報
  short CountDisc();
  // 盤面取得
  BitBoard GetBoard(bool);
  // 盤面表示
  void PrintBoard();
};

#endif
