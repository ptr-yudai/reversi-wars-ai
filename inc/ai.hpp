/*
 * AIの定義
 */
#include <fstream>
#include <stdarg.h>
#include "board.hpp"

#define DISC_EVAL        1000
#define MAX_PATTERN_EVAL (DISC_EVAL * 20)
#define ALPHA_UPDATE     0.005

class AI
{
private:
  enum PATTERN {
    DIAG4, DIAG5, DIAG6, DIAG7, DIAG8,
    EDGE2X,
    HV2, HV3, HV4,
    CORNER25, CORNER33,
    END_OF_PATTERN
  };
  int PATTERN_SIZE[PATTERN::END_OF_PATTERN] = {
    81, 243, 729, 2187, 6561,
    59049,
    6561, 6561, 6561,
    59049, 19683
  };
  
  Board *board;    // 盤面へのポインタ
  bool isBlack;    // 自分の石の色
  
  short mid_depth; // 中盤の先読み数
  short wld_depth; // 必勝先読み数
  short ex_depth;  // 完全先読み数

  int *eval_pattern[PATTERN::END_OF_PATTERN]; // パターンによる評価値
  int mirror_line[6561], mirror_corner[6561]; // あるマスの対象および180度反対のマス
  std::string pattern_file; // パターン評価値を保存するファイル名

  int CountBit(BitBoard); // ビット数をカウント
  void UpdateAllPattern(int, bool, BitBoard, BitBoard);
  void UpdatePattern(int, int, int, int);
  template<class... T> int CalcIndex(bool, BitBoard, BitBoard, T...);
  int EvalPattern(bool, BitBoard, BitBoard);
  int EvalDisc(bool, BitBoard, BitBoard);
  int EndSearch(bool, BitBoard, BitBoard, short, int, int, BitBoard&);

public:
  AI(Board*, bool);
  void SetParameter(short, short, short);
  bool Ponder(short&, short&); // 思考
  void LoadPattern(); // パターンをロード
  void SavePattern(); // パターンをセーブ
  ~AI();
};
