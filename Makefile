CXXINCLUDES = -I./inc
CXX = g++
TARGET = ./bin/reversi
CXXFLAGS = -Wall -O3 -std=c++11
SRC = ./src/main.cpp ./src/board.cpp ./src/client.cpp ./src/ai.cpp
OBJ := $(SRC:.cpp=.o)

$(TARGET): $(OBJ)
	$(CXX) $(CXXFLAGS) $^ -o $@

%.o: %.cpp
	$(CXX) $(CXXINCLUDES) $(CXXFLAGS) -c $< -o $@

clean:
	rm -f $(TARGET) $(OBJ)
	rm -f ./bin/*
